package com.energystar;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import java.util.Scanner;

public class Main {
    private static SerialPort serialPort;

    public static void main(String[] args) {
        serialPort = new SerialPort("COM10");
        try {
            //Открываем порт
            if (serialPort.isOpened()) {
                serialPort.closePort();
            }
            serialPort.openPort();
            //Выставляем параметры
            serialPort.setParams(SerialPort.BAUDRATE_9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);
            //Устанавливаем ивент лисенер и маску
            serialPort.addEventListener(new PortReader(), SerialPort.MASK_RXCHAR);
            //Отправляем запрос устройству
            serialPort.writeString("Hello World!");
        } catch (SerialPortException ex) {
            System.out.println(ex);
        }


    }

    private static class PortReader implements SerialPortEventListener {

        public void serialEvent(SerialPortEvent event) {
            if (event.isRXCHAR() && event.getEventValue() > 0) {
                try {
                    //Получаем ответ от устройства, обрабатываем данные и т.д.
                    String data = serialPort.readString(event.getEventValue());
                    System.out.println(data);
                    Scanner keyboard = new Scanner(System.in);
                    String myS = keyboard.nextLine();
                    serialPort.writeString(myS);
                } catch (SerialPortException ex) {
                    System.out.println(ex);
                }
            }
        }
    }
}
